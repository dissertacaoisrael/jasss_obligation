Read me file for the simulation on obligation norm identification.

Objective:
==========
The objective of this simulation program is to demonstrate that norms can identified using
observation based learning. We have chosen signalling to be the top level construct. We assume
that an agent can signal another agent's wrong doing by sanctioning it. We assume that sanctions
are recognizable. Based on recognizing a sanction, an agent can infer what the potential norms
of the society are.

In this simulation, we demonstrate how one of the deontic norms, the obligation norm can be
identified. For this purpose we have simulated a virtual restaurant where agents can perform
certain actions (arrive, occupy a seat and order food, eat, pay, tip and sanction). There are three
types of agents (tipping customer, non tipping customer and a waiter). Agents that do not tip may
be sanctioned by the waiter agent. The simulation demonstrates how an agent can use a sequence
mining approach to identify the obligation norm of tipping.

Overview of the classes used in this simulation program:
========================================================
The simulation makes use of the MVC architecture. There are two packages (domain and gui packages).
The "domain" package contains the following files.

1. Agent.java -> An abstract class that has most of the agent functionalities. This is extended by
the NTCAgent, TCAgent and WaiterAgent classes.
2. NTCAgent -> Class representing a non tipping customer
3. TCAgent -> Class representing a tipping customer
4. WaiterAgent -> Class representing a waiter agent
5. SimConst.java -> Class containing all the simulation parameters. The parameters can be changed.
6. WorldModel.java -> The model of the MVC architecture.
7. ONI_algorithm.java -> This class contains the sequence mining algorithm that is used by all the
agents.
8. Simulator.java -> This is the main class from which the simulation should be run. This is the
"controller" of the MVC architecture.

The "gui" package contains the following two classes

1. SimFrame.java -> A JFrame class that contains a panel on which the graphics are drawn.
2. SimJPanel.java -> The "view" part of the MVC architecture.  On this JPanel, the graphical
objects are drawn.

Where to find more information:
===============================
The paper that describes this simulation has been accepted for publication with the JASSS journal.
You can read the paper by searching for the title of the paper on the Internet. The title of the
paper is "Obligation norm identification in agent societies".

Disclaimer:
==========
The authors note that the simulation program describes a simple approach for norm identification.
The approach can be modified and extended in many ways (please read the paper for more information).
However, we note that the main contribution of this simulation is that it demonstrates that agents
can learn norm by making use of a data mining approach.