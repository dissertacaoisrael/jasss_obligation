/* Agent is an abstract class. The abstract method is the chooseAction method which varies from an agent to agent depending upon what the agent wants to do
 */
package domain;

import domain.SimConst.Action;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 *
 * @author TonyR
 */
public abstract class Agent {
    int id; //ID of an agent
    int curXPos = -1; // Current X position of an agent
    int curYPos = -1; // Current X position of an agent
    int curZone = -1; // Current zone of an agent
    String personality; //Personality type
    SimConst.Action currentAction = null; //Current action that an agent takes
    int entryIteration = SimConst.myRand.nextInt(SimConst.hiatus); //iteration in which an agent enters the simulation
    String actionHistory[][] = new String[SimConst.historyLen][SimConst.noOfAgents]; //stores action history
    int normsNotFoundForIterations = 0; //A norm has not found for certain number of iterations (incremented every iteration)

    List<String> normsList = new ArrayList<String>();
    Dimension nextSeatingLocation = null;
    String prevMovementDirection = "START";
    int yPosForPath = -1;
    List<String> candidateNormsList = null;


    public int getEntryIteration() {
        return entryIteration;
    }

    public void setEntryIteration(int entryIteration) {
        this.entryIteration = entryIteration;
    }

    public int getCurXPos() {
        return curXPos;
    }

    public void setCurXPos(int curXPos) {
        this.curXPos = curXPos;
    }

    public int getCurYPos() {
        return curYPos;
    }

    public void setCurYPos(int curYPos) {
        this.curYPos = curYPos;
    }

    public Action getCurrentAction() {
        return currentAction;
    }

    public void setCurrentAction(Action currentAction) {
        this.currentAction = currentAction;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPersonality() {
        return personality;
    }

    public void setPersonality(String personality) {
        this.personality = personality;
    }

    public int getCurZone() {
        return curZone;
    }

    public void setCurZone(int curZone) {
        this.curZone = curZone;
    }

    public abstract SimConst.Action chooseAction(SimConst.Action currentAction);

    //Method for an agent to add a norm
    public void addNorm(String candidateNorm, int iterNo) {
        boolean normExists = false;

        for (int i = 0; i < normsList.size(); i++) {
            if (normsList.get(i).equals(candidateNorm)) {
                normExists = true;
                break;
            }
        }

        if (!normExists) {
            //System.out.println("Adding norm for agent " + id + " in iteration " + iterNo + " :" + candidateNorm);
            normsList.add(candidateNorm);
        }
    }

    //Method to delete all the norms
    public void deleteNorms() {
        normsList.clear();
    }

     //Method to initialize the location
    public void initializeLocation() {        
            Dimension dim = null;
            if(currentAction== null || currentAction==Action.ARRIVE) {
                dim = findNextLocation();
                  this.curXPos = dim.width;
                  this.curYPos = dim.height;
                setCurZone(getZone(curXPos, curYPos));
            }
    }

     //Method for an agent to perform an action
    public void performAction(int agentId, SimConst.Action action, int iterNo, Dimension nextSeat) {
        this.currentAction = action;
        if(!personality.startsWith("W")) {
            if (iterNo >= this.getEntryIteration()) {
                if (action == Action.ARRIVE) {
                    if (nextSeatingLocation == null) {
                        //System.out.println("Seat for agent " + agentId + " is " + nextSeat.width + " " + nextSeat.height + " zone is " + curZone);
                        nextSeatingLocation = nextSeat;
                    } else {
                        findNextLocation();
                    }
                }
            }
            if(action == Action.DEPART){
                if(yPosForPath == -1) {
                    Dimension startPathsArr[] = WorldModel.getAllPathStartsWithinZone(curZone);
                    Dimension closestStartPath = getClosestPath(curYPos, startPathsArr);
                     this.curYPos = closestStartPath.height;
                     yPosForPath = this.curYPos;
                }
                else {
                    if (curXPos == SimConst.noOfAgents - 1) {
                        currentAction = null;
                        nextSeatingLocation = null;
                        prevMovementDirection = "START";
                        yPosForPath = -1;
                    }
                    else {
                        this.curXPos = this.curXPos+1;
                    }
                }
            }
        }
        
    }

     //Method to find the next location to move to.
    private Dimension findNextLocation() {
        
        if (curXPos == -1) {
            return new Dimension(0, SimConst.myRand.nextInt(SimConst.noOfAgents));
        } else if (curXPos == SimConst.noOfAgents - 1) {
            return getNextAppearanceLocationInTheSameSociety();
        } else {
            return findNextLocationTowardsSeat(nextSeatingLocation.width, nextSeatingLocation.height);
        }
    }

     //Method to find the next location within the same society
    private Dimension getNextAppearanceLocationInTheSameSociety () {
        if(this.getZone(curXPos,curYPos) == 1) {
            return new Dimension (0, SimConst.myRand.nextInt(SimConst.noOfAgents/2));
        }
        else if(this.getZone(curXPos,curYPos) == 2) {
            return new Dimension(0,(SimConst.myRand.nextInt(SimConst.noOfAgents/2)+ SimConst.noOfAgents/2));
        }
        else {
            return null;
        }
    }

     //Method to find the zone.
    public int getZone(int x, int y) {
        int maxXYPos = SimConst.noOfAgents - 1; //This is the maximum X and Y Pos an agent can have

        int midXYPos = (SimConst.noOfAgents / 2) - 1;
        int minXYPos = 0;
        int zone = -1;

        if (isWithin(minXYPos, minXYPos, maxXYPos, midXYPos, x, y)) {
            zone = 1;
        } else if (isWithin(minXYPos, midXYPos, maxXYPos, maxXYPos, x, y)) {
            zone = 2;
        }
        return zone;
    }

    //Method to check if an agent is within certain X, Y coordinates (zone)
    public boolean isWithin(int x1, int y1, int x2, int y2, int x, int y) {

        if (x >= x1 && x <= x2 && y >= y1 && y <= y2) {
            return true;
        } else {
            return false;
        }
    }

    //Method to move towards the seat location that the agent has chosen
    private Dimension findNextLocationTowardsSeat(int seatXPos, int seatYPos) {

        //check if I am next to my chosen seat, then sit
        if ((curYPos - seatYPos == 1 || curYPos - seatYPos == -1) && curXPos == seatXPos) {
            return new Dimension(seatXPos, seatYPos);
        } else if (curXPos == seatXPos && curYPos == seatYPos) {
            return new Dimension(seatXPos, seatYPos);
        } else {
            //else move towards the seat
            Dimension startPathsArr[] = WorldModel.getAllPathStartsWithinZone(curZone);
            Dimension closestStartPath = getClosestPath(seatYPos, startPathsArr);
            
            seatXPos = closestStartPath.width;
            seatYPos = closestStartPath.height;

            if (curYPos > seatYPos && !prevMovementDirection.contains("NIL")) {
                prevMovementDirection = "UP";
                return new Dimension(curXPos, curYPos - 1);
            } else if (curYPos < seatYPos && !prevMovementDirection.contains("NIL")) {
                prevMovementDirection = "DOWN";
                return new Dimension(curXPos, curYPos + 1);
            } else if (curYPos == seatYPos) {
                if (prevMovementDirection != null && prevMovementDirection.equals("UP")) {
                    prevMovementDirection = "NILUP";
                    return new Dimension(curXPos, curYPos - 1);

                } else if (prevMovementDirection != null && prevMovementDirection.equals("DOWN")) {
                    prevMovementDirection = "NILDOWN";
                    return new Dimension(curXPos, curYPos + 1);
                }
                 else {
                    return new Dimension(curXPos + 1, curYPos);
                }
            } 
            else if(prevMovementDirection.equals("NILUP")) {
                return new Dimension(curXPos, curYPos - 1);
            }
            else if(prevMovementDirection.equals("NILDOWN")) {
                return new Dimension(curXPos, curYPos + 1);
            }
            else {
                return new Dimension(curXPos + 1, curYPos);
            }
        }
    }

    //Method to get the closest path
    private Dimension getClosestPath(int seatYPos, Dimension[] startPathsArr) {

        for (int i = 0; i < startPathsArr.length; i++) {
            Dimension dimension = startPathsArr[i];
            if (Math.abs(seatYPos - dimension.getHeight()) == 1) {
                return dimension;
            }
        }

        return null;
    }

    //Agent records the action of another agent in the action history
    public void recordAction(int agentNo, String actionStr, int iterNo, int agentXPos, int agentYPos) {
        iterNo = iterNo%SimConst.historyLen;
        
        //if agentNo is in the same zone as me then I would record it
        if(this.getZone(agentXPos, agentYPos) == curZone && Math.abs(curXPos-agentXPos)<=SimConst.visibilityThreshold && Math.abs(curYPos-agentYPos)<=SimConst.visibilityThreshold) {
            actionHistory[iterNo][agentNo] = actionStr;
        }   
    }

    //Clears the action history (history of events)
    public void cleanActionHistory() {
        for (int i = 0; i < SimConst.historyLen; i++) {
            for (int j = 0; j < SimConst.noOfAgents; j++) {
                actionHistory[i][j] = "";
                
            }
            
        }
    }

    //Method to print the action history
    public void printActionHistory() {
        for(int i=0; i<SimConst.historyLen; i++) {
        for (int j = 0; j < SimConst.noOfAgents; j++) {
            System.out.print(actionHistory[i][j] + "#");
        }
        System.out.println("");
        }
    }

    //Generate Event Episode List from action history
    public List generateEEL() {

        List eventEpisodeList = new ArrayList();
        String episode = "";

        for (int i = 0; i < SimConst.noOfAgents; i++) {
            for (int j = 0; j < SimConst.historyLen; j++) {
                String action = actionHistory[j][i];
                if (action != null && !action.equals("")) {
                    if (episode.equals("")) {
                        episode += String.valueOf(action.charAt(0));
                    } else {
                        if (episode.charAt(episode.length() - 1) != action.charAt(0) && episode.charAt(episode.length() - 1) != 'S') {
                            episode += String.valueOf(action.charAt(0));
                        }
                    }
                }
            }
            if (!episode.equals("")) {
                eventEpisodeList.add(episode);
                episode = "";
            }
        }
        return eventEpisodeList;
    }

    //Method to print event episode list
    public void printEEL(List l){
        for (int i = 0; i < l.size(); i++) {
            System.out.println((String)l.get(i));
            
        }
    }

    //Method to infer norms
    public List inferNorms(String tempEELArr[]) {
        int noOfEventsBeforeSanction = SimConst.windowSize;
        ONI_algorithm oni = new ONI_algorithm(tempEELArr);
        String tempNEELArr[] = oni.getNEELArr(tempEELArr);
        String tempSEELArr[] = oni.getSEELArr(tempEELArr);
        //To restrict the size of the SEEL that needs to be considered to be the Window Size, comment out the previous line and uncomment the following line
        //String tempSEELArr[] = oni.getSEELArr(tempEELArr,noOfEventsBeforeSanction);
        
        // This is the first pass of the ONI algorithm which is to find event episodes of length=WS, that precede a sanction
        oni.computeCandidateNorms(tempSEELArr, SimConst.NITa, noOfEventsBeforeSanction);
        List<String> chosenEEFromTempSEELArr = chooseEEfromTempSEELArr(oni.sortMap(), noOfEventsBeforeSanction);
     
        candidateNormsList = new ArrayList();
        for (int i = 0; i < chosenEEFromTempSEELArr.size(); i++) {
            String tempStr = chosenEEFromTempSEELArr.get(i);
            String tempArr[] = oni.getSuperSequencesFromNEEL(tempNEELArr, 0, tempStr);
            if(tempArr!=null) {
                //This is the second pass of the ONI algorithm which is to find the sub-sequences that are frequent from the tempArr (superSequences from NEEL)
                oni.computeCandidateNorms(tempArr, SimConst.NITb, noOfEventsBeforeSanction+1); //+1 because the supersequence should have atleast one action more than one action than the subsequence
                oni.sortAndPrintCandiateNorms(tempStr);
                List<String> chosenEEFromTempNEELArr = chooseEEfromTempNEELArr(tempStr, oni.getAllCandiateNorms(tempStr),noOfEventsBeforeSanction+1);
                candidateNormsList.addAll(chosenEEFromTempNEELArr);
                candidateNormsList = removeDuplicate(candidateNormsList);
            }
        }
        System.out.println("Candidate norms list of agent " + id + ":" + candidateNormsList);
        return candidateNormsList;
    }

   //Method to extract events that happen before a sanctioning event
   private List chooseEEfromTempSEELArr(String sortedArr[][], int noOfEventsBeforeSanction) {
       List chosenEEFromSEELArr = new ArrayList();
       
            for (int i = 0; i < sortedArr.length; i++) {
               if(sortedArr[i][0].length() == noOfEventsBeforeSanction) { // == can be changed to <= if all subepisodes are to be considered
                   chosenEEFromSEELArr.add(sortedArr[i][0]);
               }    
            }
       return chosenEEFromSEELArr;
    }

   //Method to extract events that are supersequences that contain a EE (tempStr in this method)
      private List chooseEEfromTempNEELArr(String tempStr, List<String> sortedList, int noOfEventsBeforeSanction) {
       
       List<String> chosenEEFromSEELArr = new ArrayList();
       
            for (int i = 0; i < sortedList.size(); i++) {
               if((sortedList.get(i)).length() == noOfEventsBeforeSanction) {
                   chosenEEFromSEELArr.add(findObligedAction(tempStr,sortedList.get(i)));
               }    
            }
       return chosenEEFromSEELArr;
    }

      //Finding the obliged action (e.g. Finding that EPTD is a supersequence of EPD, hence T is a norm).
      public String findObligedAction(String fromSEEL, String fromNEEL) { //fromSEEL = "EPD" fromNEEL = "EPTD"
          String tempStr = "";
          for(int i=0; i< fromSEEL.length(); i++) {
              tempStr = String.valueOf(fromSEEL.charAt(i));
              if(fromNEEL.contains(tempStr)) {
                  fromNEEL = fromNEEL.replace(tempStr,"");
              }
          }
          return fromNEEL;          
      }

     //Removing duplicate candidate norms
    public List removeDuplicate(List arlList) {
        HashSet h = new HashSet(arlList);
        arlList.clear();
        arlList.addAll(h);
        return arlList;
    }

    //Check if an agent has a candidateNorm
    public boolean checkNorm(String candidateNorm) {
        boolean returnVal = false;

        if(normsList!=null) {
            for (int i = 0; i < normsList.size(); i++) {
                //System.out.println("Checking " + candidateNorm + " Norms of agent " + normsList.get(i));
                if(normsList.get(i).equals(candidateNorm)) {
                    return true;
                }

            }
        }
        return returnVal;
    }
        
    public void incrementNoNormsFoundCounter(){
        normsNotFoundForIterations++;
    }
    
    public void reinitializeNormsNotFoundCounter(){
        normsNotFoundForIterations = 0;
    }
    
    public int getNormsNotFoundForIterations(){
        return normsNotFoundForIterations;
    }

}
