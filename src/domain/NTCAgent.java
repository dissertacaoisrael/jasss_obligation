/* Code of the non tipping customer agent
 */

package domain;

import domain.SimConst.Action;

/**
 *
 * @author TonyR
 */
public class NTCAgent extends Agent {
    
    int randNo;
    int counter =0;

    NTCAgent(int id) {
        this.id = id;
        this.personality = "NTC";
        randNo = SimConst.myRand.nextInt(SimConst.eatingTime);
    }

    // The mechanism for an non tipping customer agent to choose an action
    @Override
    public Action chooseAction(Action currentAction) {
        if(currentAction== null || (currentAction == Action.ARRIVE && !(curXPos==nextSeatingLocation.width && curYPos == nextSeatingLocation.height))) {
                return Action.ARRIVE;
        }
        else if(currentAction == Action.ARRIVE  && (curXPos==nextSeatingLocation.width && curYPos == nextSeatingLocation.height)){
            return Action.OCCUPY;  
        }
        else if(currentAction.equals(Action.OCCUPY)) {
            return Action.EAT;
        }
        else if (currentAction.equals(Action.EAT)) {
            if(counter < randNo) {
                counter++;
                return Action.EAT;   
            }
            else
            {
                counter =0;
                return Action.PAY;
            }
        }
         else {
            return Action.DEPART;
        }
    }

}
