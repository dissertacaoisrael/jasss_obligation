package domain;

/*
 * This class contains provides the sequence mining technique to identify obligation norms.
 */
import java.lang.String;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author TonyR
 */
public class ONI_algorithm {
    Map<String, Double> cns = null; //candidate norm set
    String seqArr[] = null; //sequence array
    double minsup = -1; //minimum support (norm inference threshold, NIT)
    String eventTypes = null; //will hold a unique event set
    String tempEELArr[] = null;
    
    public ONI_algorithm(String EELArr[]) {
        tempEELArr = EELArr;
    }
    
    public void computeCandidateNorms(String[] seqArr, double minsup, int lengthOfEpisodes) {
        cns = new HashMap<String, Double>();
        this.seqArr = seqArr;
        this.minsup = minsup;
        
        eventTypes = identifyEventTypes();   
        List<String> candidateLists[] = new ArrayList[lengthOfEpisodes]; //An array of arraylists

        candidateLists[0] = new ArrayList<String>();
        for(int i=0; i<eventTypes.length();i++) {
            candidateLists[0].add(Character.toString(eventTypes.charAt(i)));
        }
        candidateLists[0] = getMinimumSupportList(candidateLists[0]);
      
        for (int i = 2; i <= lengthOfEpisodes; i++) {
            candidateLists[i-1] = generateCandidateLists(candidateLists[i-2],i-1);
            candidateLists[i-1] = getMinimumSupportList(candidateLists[i-1]);
        }
    }
    
    //Generate candidates based on the list from previous level (for level 2, use level 1 information
    private List<String> generateCandidateLists(List<String> prevLevelList, int level) {	
    	String prevListArr[] = prevLevelList.toArray(new String[prevLevelList.size()]);
    	List<String> candidateList = new ArrayList<String>();
    	
    	for (int i = 0; i < prevListArr.length; i++) {
            String temp = prevListArr[i];
            String matchStr = temp.substring(1); //match string should be any string starting after the first letter

            for (int j = i+1; j < prevListArr.length; j++) {
                            if(prevListArr[j].startsWith(matchStr)) {
                                    candidateList.add(temp.charAt(0) + prevListArr[j]);
                            }
                    }

            for (int k = 0; k <= i; k++) {
                    if(prevListArr[k].startsWith(matchStr)) {
                                    candidateList.add(temp.charAt(0) + prevListArr[k]);
                            }
                    }
            }
            return candidateList;
	}


        //Method to find event episodes that have minimum support for evidence (that have frequencies greater than NIT (minsup))
	public List getMinimumSupportList(List<String> list) {
            List<String> tempList = new ArrayList<String>();

            for (String s : list) {
                int minsupEvidence = 0;
                for (int i = 0; i < seqArr.length; i++) {
                    if (seqArr[i].contains(s)) {
                        minsupEvidence++;
                    }
                }
                double temp = roundDouble((((double) (minsupEvidence) / (double) (seqArr.length)) * 100),2);

                if (temp >= minsup) {
                    tempList.add(s);
                    cns.put(s,temp);
                }
            }
            return tempList;
    }

    //method to sort and print candidate norms
    public void sortAndPrintCandiateNorms(String subSequence) {
         String sortedArr[][] = sortMap();
        for (int x = 0; x < sortedArr.length; x++) {
            if(subSequence.equals("")) {
                System.out.println(sortedArr[x][0] + " " + sortedArr[x][1]);
            }
            else
            {
                if(isSubsequence(subSequence, sortedArr[x][0])){
                    System.out.println(sortedArr[x][0] + " " + sortedArr[x][1]);
                }
            }
        }
         System.out.println("Size of sorted arr " + sortedArr.length);
    }

    //Method to find all candidate norms
    public List getAllCandiateNorms(String subSequence) {
         String sortedArr[][] = sortMap();
         List candidateNormsList = new ArrayList();
        for (int x = 0; x < sortedArr.length; x++) {
            if(subSequence.equals("")) {
              
                candidateNormsList.add(sortedArr[x][0]);
            }
            else
            {
                if(isSubsequence(subSequence, sortedArr[x][0])){
                    candidateNormsList.add(sortedArr[x][0]);
                 
                }
            }
        }
         return candidateNormsList;
    }
    
     
    //Sorting the map
    public String[][] sortMap() {
        String sortedArr[][] = new String[cns.size()][2];

        List<Double> uniqueValuesList = new ArrayList<Double>();
        for (Map.Entry<String, Double> e : cns.entrySet()) {
            if (!uniqueValuesList.contains(e.getValue())) {
                uniqueValuesList.add(e.getValue());
            }
        }

        Comparator comparator = Collections.reverseOrder();
        Collections.sort(uniqueValuesList, comparator);

        int counter = 0;
        for (Double double1 : uniqueValuesList) {
            for (Map.Entry<String, Double> e : cns.entrySet()) {
                if (double1.equals(e.getValue())) {
                    sortedArr[counter][0] = e.getKey();
                    sortedArr[counter][1] = "" + e.getValue();
                    counter++;
                }
            }
        }
        return sortedArr;
    }
    
    //method to find the unique event set
    public String identifyEventTypes() {
        eventTypes = "";
        String sequenceArray[] = this.tempEELArr;
        for (int i = 0; i < sequenceArray.length; i++) {
            String tempStr = sequenceArray[i];
            int tempStrLen = tempStr.length();
            for (int j = 0; j < tempStrLen; j++) {
                String tempChar = Character.toString(tempStr.charAt(j));
                if(!eventTypes.contains(tempChar)){
                    eventTypes=eventTypes+tempStr.charAt(j);
                }
            }
        }
        return eventTypes; 
    }

    //method for rounding based on the number decimal digits needed.
    public static final double roundDouble(double d, int places) {
        return Math.round(d * Math.pow(10, (double) places)) / Math.pow(10,
            (double) places);
    }

    //Get an array of episodes that do not contain sanctions as one of the events
    public String[] getNEELArr(String[] tempEELArr) {
        List<String> tempNEEL = new ArrayList<String>();
        for (int i = 0; i < tempEELArr.length; i++) {
            String tempStr = tempEELArr[i];
            if(!tempStr.contains("S")){
                tempNEEL.add(tempStr);
            }      
        }
        System.out.println("NEEL array is " + tempNEEL);
        return tempNEEL.toArray(new String[tempNEEL.size()]);
    }


    //Get an array of episodes that have sanctions as one of the events
    public String[] getSEELArr(String[] tempEELArr) {
        List<String> tempSEEL = new ArrayList<String>();
        for (int i = 0; i < tempEELArr.length; i++) {
            String tempStr = tempEELArr[i];
            if(tempStr.contains("S")){
                tempStr = tempStr.replace("S","");
                tempSEEL.add(tempStr);
            }  
        }
        System.out.println("SEEL array is " + tempSEEL);
        return tempSEEL.toArray(new String[tempSEEL.size()]);
    }


    //Get an array of episodes that have sanctions as one of the events
    public String[] getSEELArr(String[] tempEELArr, int sizeOfWindow) {
        List<String> tempSEEL = new ArrayList<String>();
        for (int i = 0; i < tempEELArr.length; i++) {
            String tempStr = tempEELArr[i];
            if(tempStr.contains("S")){
                tempStr = tempStr.replace("S","");
                if(tempStr.length()> sizeOfWindow) {
                    tempStr = tempStr.substring(tempStr.length()- sizeOfWindow);
                }
                tempSEEL.add(tempStr);
            }
        }
        System.out.println("SEEL array is " + tempSEEL);
        return tempSEEL.toArray(new String[tempSEEL.size()]);
    }

    //Get the supersequences of a  sequence (subSequence variable) from an array (String[]).
    public String[] getSuperSequencesFromNEEL(String[] NEELArr, int minsup, String subSequence) {
        List<String> tempEEList = new ArrayList<String>();
      
        for (int i = 0; i < NEELArr.length; i++) { 
            if(isSubsequence(subSequence,NEELArr[i])){
                tempEEList.add(NEELArr[i]);
            }

        }
        System.out.println("Super sequence of " + subSequence + " is " + tempEEList.toString());
            return tempEEList.toArray(new String[tempEEList.size()]);
       
    }
    
  //Check if String s is a subsequence of t
    public boolean isSubsequence(String s, String t) {
        int M = s.length();
        int N = t.length();

        int i = 0;
        for (int j = 0; j < N; j++) {
            if (s.charAt(i) == t.charAt(j)) i++;
            if (i == M) return true;
        }
        return false;
    }
    
}
