/* This class stores all the parameters of the system (the ones that need to be changed for the experiments are the historylen, referral, NIT parameters and the visibility threshold
 */
package domain;

import java.awt.Color;
import java.util.Random;

/**
 *
 * @author TonyR
 */
public class SimConst {

 // Agent related parameters
    public static final int noOfAgents = 50; //Total number of agents
    public static final int noOfTC = 25; //Tipping customers
    public static final int noOfNTC = 21; //Non Tipping customer
    public static final int noOfWaiters = 4; //No. of. waiters

 //GUI related parameters
    public static final int gridSize = 800; //Size of the grid in pixels
    public static final int sizeOfCell = gridSize/noOfAgents; //Size of each cell
    static boolean initValuesCreated = false; //Are the initial values created?  False->it creates new initial values every time. True-> Reuses initial values

 //Color related parameters
    public static final Color TCColor = Color.GREEN; //Color of the tipping agent
    public static final Color NTCColor=  Color.RED; //Color of the non-tipping agent
    public static final Color WColor = Color.BLUE; //Color of the waiter agent
    public static final Color TipNormColor = Color.BLUE; //Color that appears inside the agent once it has found the norm of tipping
    
 //iterations
    public static final int iterations = 1000; //Total number of iterations

 // Increasing and decreasing the speed of the simulation and the arrival rate of agents
    static long sleep = 1; //If you want to make the simulation visualization faster set this to 0. Otherwise set it to 1 or any higher number.
    static int hiatus = 50; //Represents a random variable for the arrival rate (agents randomly appear in the first 50 iterations)

 //These are the parameters that needd to be varied for various experiments. They are history length, referral, NIT parameters, visibility threshold, window size
    static final int historyLen = 50; //History Length (HL)
    static final int maxReferralNo =10; //Maximum number of referral (Ref)
    static final int NITa=100;
    static final int NITb=50;

    static final int visibilityThreshold = 15; //Visibility of an agent
    static final int windowSize = 3; //Window Size (WS)

    // Parameter for allowing norm deletion in agents when no norm is found after certain number of inferences
    static final boolean normDeletionAllowed = false;
    static final int normDeletionThreshold = 5 ; //If norm deletion is allowed, then delete all norms if no norm found after 5 iterations, if needed has to be uncommented.

    //The following are for when the punishers start and stop punishment. If these are needed appropriate values have to be set
    static final int noPunishmentStartIter = -1;
    static final int noPunishmentEndIter = -1;

   //Action allowed in the system
    public static enum Action {ARRIVE, OCCUPY, EAT, PAY, TIP, DEPART,SANCTION,WAIT};

    //An agent eats for certain number of time steps
    public static int eatingTime = 10;
    
    //Punishing probability of a waiter for not tipping (from 0 to 1)
    public static double punishProbForNonTipping = 1;
    
    //Parameters corresponing to setting up tables and chairs
        public static final int noOfTableRows = 4;
        public static final int noOfTableColumns = 3;
        public static final int noOfChairs = 72;
        public static final int start_row_tables = 6; //Starting row for the tables
        public static final int table_width = 5; //Width of each table
        public static final int dbt_y = 6; //distance between tables on the y axis
        public static final int dbt_x = 8 ; //distance between tables on the x axis
    
     //Parameter to set the randomness of the experiments (keeping the seed the same will guarantee reproduction of the results as long as the initial values are the same (initValuesCreated = true))
        public final static Random myRand = new Random(9000);
}
