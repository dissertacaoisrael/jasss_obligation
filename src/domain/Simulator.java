
/**
 * The simulation uses MVC architecture.
 * Simulator is the controller class in the MVC architecture
 */

package domain;

import domain.SimConst.Action;
import gui.SimFrame;
import java.awt.Dimension;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class Simulator {

    //Initialize agents, chiars, tables
    private Agent agents[] = new Agent[SimConst.noOfAgents];
    private int chairLocs[][] = new int[SimConst.noOfChairs][4]; //xPos,yPos,zone,availability
    private int tableLocs[][] = new int[SimConst.noOfTableRows * SimConst.noOfTableColumns][4];
    //normsData holds the norms of individual agents
    private Map<Integer, Integer> normsData = new TreeMap<Integer, Integer>();
    private WorldModel model = null;
    private SimFrame sf = null;

    //Method to initialize the model and the view
    public void init() {
        model = new WorldModel();
        sf = new SimFrame(model);
        sf.setVisible(true);
    }

    //Main method
    public static void main(String args[]) {
        Simulator sim = new Simulator();
        sim.init();
        sim.initializeChairsAndTables();
        sim.createAgents();
        for (int i = 0; i < SimConst.iterations; i++) {
            sim.simulate(i);
            sim.model.setIterationNo(i);
        }

        //print details
        sim.printTippingData();

    }

    //Method to initialize tables and chairs
    public void initializeChairsAndTables() {
        int tempX = SimConst.dbt_x;
        int tempY = SimConst.dbt_y;
        int counter = 0;
        int table_counter = 0;
        for (int i = 0; i < SimConst.noOfTableRows; i++) {
            for (int j = 0; j < SimConst.noOfTableColumns; j++) {

                for (int k = 0; k < SimConst.table_width; k += 2) {
                    chairLocs[counter][0] = tempX + k;
                    chairLocs[counter][1] = tempY - 1;
                    chairLocs[counter][2] = getZone(chairLocs[counter][0], chairLocs[counter][1]);
                    chairLocs[counter][3] = 0;
                    counter++;

                    chairLocs[counter][0] = tempX + k;
                    chairLocs[counter][1] = tempY + SimConst.table_width;
                    chairLocs[counter][2] = getZone(chairLocs[counter][0], chairLocs[counter][1]);
                    chairLocs[counter][3] = 0;
                    counter++;
                }
                tableLocs[table_counter][0] = tempX;
                tableLocs[table_counter][1] = tempY;
                tableLocs[table_counter][2] = SimConst.table_width;
                tableLocs[table_counter][3] = SimConst.table_width;
                
                table_counter++;
                tempX = SimConst.dbt_x * (j + 2) + SimConst.table_width * (j + 1);
            }
            tempX = SimConst.dbt_x;
            tempY = SimConst.dbt_y * (i + 1) + SimConst.table_width * (i + 1) + SimConst.start_row_tables;
        }

        //Printing chair locations
        /*
            for (int i = 0; i < chairLocs.length; i++) {
                System.out.println(chairLocs[i][0] + " " + chairLocs[i][1] + " " + chairLocs[i][2] + " " + chairLocs[i][3]);
            }
        */
         
    }

    //Method to create agents (If initValuesCreated == false, new values are created and stored in file, if true old values are used)
    public void createAgents() {
        //Check if the number of TC + NTC + Waiters = total no of agents
        int total_agents = SimConst.noOfTC + SimConst.noOfNTC + SimConst.noOfWaiters;
        System.out.println(total_agents + " " + SimConst.noOfAgents);
        if (total_agents != SimConst.noOfAgents) {
            JOptionPane.showMessageDialog(null, "The number of agents in the system do not match with personalities");
            System.exit(0);
        } else {
            if (!SimConst.initValuesCreated) {
                initializeAgents();

            } else {
                initializeAgentsUsingPrevValues(getAgentPropertiesFromFile());
            }
        }
    }

    //Method to initialize agents
    public void initializeAgents() {
        int counter = 0;

        for (int i = 0; i < SimConst.noOfTC; i++) {
            agents[counter] = new TCAgent(counter);
            counter++;
        }

        for (int i = 0; i < SimConst.noOfNTC; i++) {
            agents[counter] = new NTCAgent(counter);
            counter++;
        }

        for (int i = 0; i < SimConst.noOfWaiters; i++) {
            agents[counter] = new WaiterAgent(counter);
            counter++;
        }

        setPositionsOfWaiters();

        model.initializeModel(tableLocs, chairLocs, this.getInitPersonalities());
        //Write properties to a file
        writeToFile();

    }

    //Setting watier positions
    public void setPositionsOfWaiters() {
        agents[46].setCurXPos(16);
        agents[46].setCurYPos(14);
        agents[46].setCurZone(1);

        agents[47].setCurXPos(31);
        agents[47].setCurYPos(14);
        agents[47].setCurZone(1);

        agents[48].setCurXPos(16);
        agents[48].setCurYPos(36);
        agents[48].setCurZone(2);

        agents[49].setCurXPos(31);
        agents[49].setCurYPos(36);
        agents[49].setCurZone(2);
    }

    //Method to write agent properties to file (to be used if experiments have to be run by varing some other parameters)
    private void writeToFile() {
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter("init.txt"));
            for (int i = 0; i < SimConst.noOfAgents; i++) {
                Agent tmpAgent = agents[i];
                out.write(tmpAgent.id + ":" + tmpAgent.personality + ":" + tmpAgent.entryIteration);
                if (tmpAgent.normsList.size() > 0) {
                    out.write(":" + tmpAgent.normsList.get(0) + ":");
                } else {
                    out.write(": :");
                }
                out.newLine();
                // System.out.println(tmpAgent.curXPos + ":" + tmpAgent.curYPos + ":" + tmpAgent.moveDir); // + ":" + tmpAgent.normsList.get(0) );
            }
            out.close();
        } catch (IOException e) {
        }
    }

    //Method to retrieve agent properties from file (init.txt).
    private String[][] getAgentPropertiesFromFile() {

        String agentProperties[][] = new String[SimConst.noOfAgents][3];
        try {
            BufferedReader in = new BufferedReader(new FileReader("init.txt"));
            String str;
            int counter = 0;
            while ((str = in.readLine()) != null) {
                agentProperties[counter] = process(str);
                counter++;
            }
            in.close();
        } catch (IOException e) {
        }

        return agentProperties;

    }

    //Method for string processing. Agent properties are comma separated in the init.txt file.
    public String[] process(String str) {

        String tmpStr[] = new String[4];
        StringTokenizer st = new StringTokenizer(str, ":");

        String id = st.nextToken();
        String personality = st.nextToken();
        String entryIteration = st.nextToken();
        String norm = st.nextToken();

        tmpStr[0] = id;
        tmpStr[1] = personality;
        tmpStr[2] = entryIteration;
        tmpStr[3] = norm;

        return tmpStr;
    }

    //Method to initialize agents with the values previously stored in the file.
    private void initializeAgentsUsingPrevValues(String[][] agentProperties) {
        //xP,yP,Dir,Personality,norm

        //int initLocsArr[][] = new int[SimConst.noOfAgents][2];
        String initPersonalityArr[] = new String[SimConst.noOfAgents];
        //int binLocsArr[][] = new int[SimConst.noOfAgents][2];

        for (int k = 0; k < SimConst.noOfAgents; k++) {
            if (agentProperties[k][1].equals("TC")) {
                agents[k] = new TCAgent(Integer.parseInt(agentProperties[k][0]));
            } else if (agentProperties[k][1].equals("NTC")) {
                agents[k] = new NTCAgent(Integer.parseInt(agentProperties[k][0]));
            } else if (agentProperties[k][1].equals("W")) {
                agents[k] = new WaiterAgent(Integer.parseInt(agentProperties[k][0]));
            }

            agents[k].addNorm(agentProperties[k][2], 0);
            initPersonalityArr[k] = agents[k].getPersonality();

        }

        setPositionsOfWaiters();
        model.initializeModel(tableLocs, chairLocs, initPersonalityArr);
    }

    //Method to get the initial personalities of agents
    public String[] getInitPersonalities() {
        String tmpPersonality[] = new String[SimConst.noOfAgents];
        for (int i = 0; i < agents.length; i++) {
            tmpPersonality[i] = agents[i].getPersonality();
        }
        return tmpPersonality;
    }

    //Method for simulation (iteration by iteration)
    private void simulate(int iterNo) {
        if (iterNo % SimConst.historyLen == 0) {
            for (int i = 0; i < agents.length; i++) {

                List tempEEL = agents[i].generateEEL();
                //identify potential obligation norm
                List candidateNormsList = agents[i].inferNorms((String[]) tempEEL.toArray(new String[tempEEL.size()]));
                //verify norms (only if not a waiter) -- waiter agent does not have to verify norm
                if (!agents[i].personality.equals("W")) {
                    verifyNorms(i, iterNo, candidateNormsList);
                }
                agents[i].cleanActionHistory();
            }

            //record norm uptake (iterNo, noOfAgentsWithTippingNorm)
            normsData.put(iterNo, getNoOfTippingAgents("obliged(T)"));
        }

        for (int i = 0; i < agents.length; i++) {
            SimConst.Action action = agents[i].chooseAction(agents[i].currentAction);
            Dimension nextSeatLocation = null;

            if (iterNo >= agents[i].getEntryIteration()) {
                if (!agents[i].personality.startsWith("W")) {
                    agents[i].initializeLocation();
                    if (agents[i].nextSeatingLocation == null) {
                        nextSeatLocation = getSeatLocation(agents[i].curZone);
                    } else {
                        nextSeatLocation = agents[i].nextSeatingLocation;
                    }

                    agents[i].performAction(i, action, iterNo, nextSeatLocation);
                    setChosenSeatLocationUnavailable(nextSeatLocation);

                    if (nextSeatLocation != null && action == SimConst.Action.DEPART) {
                        setChosenSeatLocationAvailable(nextSeatLocation);
                        nextSeatLocation = null;
                    }

                    model.setAction(i, action);
                    model.setPosition(i, agents[i].getCurXPos(), agents[i].getCurYPos());
                } else //a waiter
                {
                    //if any of the agents had not tipped in the previous iteration, then punish
                    List<Integer> punishedAgentsList = ((WaiterAgent) agents[i]).getPunishedNonTippers(iterNo);
                    for (int j = 0; j < punishedAgentsList.size(); j++) {
                       // System.out.println("Punished agent " + punishedAgentsList.get(j) + " set " + agents[punishedAgentsList.get(j)].currentAction.toString());
                        agents[punishedAgentsList.get(j)].currentAction = Action.SANCTION;
                        model.setAction(punishedAgentsList.get(j), Action.SANCTION);
                        for (int k = 0; k < agents.length; k++) {
                            agents[k].recordAction(punishedAgentsList.get(j), Action.SANCTION.toString(), iterNo, agents[punishedAgentsList.get(j)].curXPos, agents[punishedAgentsList.get(j)].curYPos);

                        }

                    }
                    if (punishedAgentsList.size() > 0) {
                        model.setAction(i, Action.SANCTION);
                    } else {
                        model.setAction(i, Action.WAIT);
                    }
                    model.setPosition(i, agents[i].getCurXPos(), agents[i].getCurYPos());
                }
            }

            //record action history
            for (int j = 0; j < agents.length; j++) {
                String curAct = (agents[i].currentAction == null) ? "" : agents[i].currentAction.toString();
                agents[j].recordAction(i, curAct, iterNo, agents[i].getCurXPos(), agents[i].getCurYPos());
            }

            try {
                Thread.sleep(SimConst.sleep);
            } catch (InterruptedException ex) {
                Logger.getLogger(Simulator.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    //Method to get the location of the seat in a table
    private Dimension getSeatLocation(int zone) {
        int counter = 0;
        for (int i = 0; i < chairLocs.length; i++) {
            if (chairLocs[i][3] == 0 && zone == chairLocs[i][2]) {
                counter++;
            }
        }

        int availableLocations[][] = new int[counter][2];
        int newcounter = 0;
        for (int i = 0; i < chairLocs.length; i++) {
            if (chairLocs[i][3] == 0 && zone == chairLocs[i][2]) {
                availableLocations[newcounter][0] = chairLocs[i][0];
                availableLocations[newcounter][1] = chairLocs[i][1];
                newcounter++;
            }
        }

        if (newcounter > 0) {
            int randLoc = SimConst.myRand.nextInt(newcounter);
            return new Dimension(availableLocations[randLoc][0], availableLocations[randLoc][1]);
        } else {
            return null;
        }
    }

    //Method to get the zone of a location (x, y).
    public int getZone(int x, int y) {
        int maxXYPos = SimConst.noOfAgents - 1; //This is the maximum X and Y Pos an agent can have

        int midXYPos = (SimConst.noOfAgents / 2) - 1;
        int minXYPos = 0;
        int zone = -1;


        if (isWithin(minXYPos, minXYPos, maxXYPos, midXYPos, x, y)) {
            zone = 1;
        } else if (isWithin(minXYPos, midXYPos, maxXYPos, maxXYPos, x, y)) {
            zone = 2;
        }
        return zone;
    }

    //Method to check if the coordinates are within a zone)
    public boolean isWithin(int x1, int y1, int x2, int y2, int x, int y) {

        if (x >= x1 && x <= x2 && y >= y1 && y <= y2) {
            return true;
        } else {
            return false;
        }
    }

    //Method to set that the location selected by an agent is unavailable.
    private void setChosenSeatLocationUnavailable(Dimension nextSeatLocation) {
        for (int i = 0; i < chairLocs.length; i++) {
            if (chairLocs[i][0] == nextSeatLocation.width && chairLocs[i][1] == nextSeatLocation.height) {
                chairLocs[i][3] = 1;
            }
        }
    }

    //Method to set that the location selected by an agent is available.
    private void setChosenSeatLocationAvailable(Dimension nextSeatLocation) {
        for (int i = 0; i < chairLocs.length; i++) {
            if (chairLocs[i][0] == nextSeatLocation.width && chairLocs[i][1] == nextSeatLocation.height) {
                chairLocs[i][3] = 0;
            }
        }
    }

    //Method to find all the locations of agents in my zone (typically this should be in the agent class)
    private List getAllAgentsInMyZone(int agentId, int myZone) {
        List agentsInSameZone = new ArrayList();
        for (int i = 0; i < agents.length; i++) {
            if (i != agentId && agents[i].curZone == myZone) {
                agentsInSameZone.add(i);
            }
        }
        return agentsInSameZone;
    }

    //Method for norm verification (asks certain number of agents. Future modification: Move this method to the agent class)
    public void verifyNorms(int agentId, int iterNo, List candidateNormList) {
        int counterMax = SimConst.maxReferralNo;
        List agentsInMyZone = getAllAgentsInMyZone(agentId, agents[agentId].curZone);

        for (int i = 0; i < candidateNormList.size(); i++) {
            int randAgent = -1;
            int randNo = -1;
            for (int counter = 0; (counter <= counterMax && agentsInMyZone.size() > 0); counter++) {
                randNo = SimConst.myRand.nextInt(agentsInMyZone.size());
                randAgent = (Integer) agentsInMyZone.get(randNo);
                String tmpStr = (String) candidateNormList.get(i);
                tmpStr = "obliged(" + tmpStr + ")";
                System.out.println("Verifying norm :" + tmpStr + "from agent " + randAgent);
                if (agents[randAgent].checkNorm(tmpStr)) {
                    agents[agentId].addNorm(tmpStr, iterNo);
                    model.addNorm(agentId, tmpStr);
                    System.out.println("Changed " + agentId + " acquired norm");
                    agents[agentId].reinitializeNormsNotFoundCounter();
                    break;
                }
            }
        }


        if(SimConst.normDeletionAllowed) {
            //If no norm is found then increment the noNormsFoundCounter
            if (candidateNormList.size() == 0) {
                agents[agentId].incrementNoNormsFoundCounter();
            }

            //If norm is not found for 5 iterations, then delete (this could be parameterized)
            if (agents[agentId].getNormsNotFoundForIterations() == SimConst.normDeletionThreshold) {
                agents[agentId].deleteNorms();
                model.deleteNorms(agentId);
                agents[agentId].reinitializeNormsNotFoundCounter();
            }
        }

    }

    //Get the total number of tipping agents
    public int getNoOfTippingAgents(String norm) {
        int counter = 0;
        for (int i = 0; i < agents.length; i++) {
            if (agents[i].checkNorm(norm)) {
                counter++;
            }
        }

        return counter;
    }

    //Method to print the tipping data
    public void printTippingData() {
        Iterator it = normsData.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pairs = (Map.Entry) it.next();
            System.out.println(pairs.getKey() + "\t" + pairs.getValue());
        }

    }
}
