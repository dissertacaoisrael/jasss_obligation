/*
        Code of the tipping customer agent
 */

package domain;

import domain.SimConst.Action;
import java.util.Random;

/**
 *
 * @author TonyR
 */
public class TCAgent extends Agent {

    int randNo;
    int counter =0;
    
    TCAgent(int id) {
        this.id = id;
        this.personality = "TC";
        randNo = SimConst.myRand.nextInt(SimConst.eatingTime);
    }

    // The mechanism for a tipping customer agent to choose an action
    @Override
    public Action chooseAction(Action currentAction) {
        
        if(currentAction== null || (currentAction == Action.ARRIVE && !(curXPos==nextSeatingLocation.width && curYPos == nextSeatingLocation.height))) {
                return Action.ARRIVE;
        }
        else if(currentAction == Action.ARRIVE  && (curXPos==nextSeatingLocation.width && curYPos == nextSeatingLocation.height)){
            return Action.OCCUPY;
            
        }
        else if(currentAction.equals(Action.OCCUPY)) {
            return Action.EAT;
        }
        else if (currentAction.equals(Action.EAT)) {
            if(counter < randNo) {
                counter++;
                return Action.EAT;
                
            }
            else
            {
                counter =0;
                return Action.PAY;
            }
        }
        else if (currentAction.equals(Action.PAY)){
            return Action.TIP;
        }
         else {
            return Action.DEPART;
        }
    }

}
