/*
 * Code of the waiter agent
 */
package domain;

import domain.SimConst.Action;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author TonyR
 */
public class WaiterAgent extends Agent {

    WaiterAgent(int id) {
        this.id = id;
        this.personality = "W";
        this.normsList.add("obliged(T)");
        this.entryIteration = 0;
    }

    // The mechanism for a waiter agent to choose an action (the action is overridden in the getPunishedNonTippers method given below).
    @Override
    public Action chooseAction(Action currentAction) {
        return Action.ARRIVE;
    }

    //When an agent leaves without paying a tip, it is punished probabilisitcally
    public List getPunishedNonTippers(int iterNo) {
        List<Integer> nonTippersList = new ArrayList<Integer>();

        if (iterNo >= SimConst.noPunishmentStartIter && iterNo <= SimConst.noPunishmentEndIter) {
            return nonTippersList;
        } else {
            int randNo = -1;

            iterNo = iterNo % SimConst.historyLen;

            if (iterNo > 3) {
                String prevActions[][] = new String[SimConst.noOfAgents][2];
                for (int i = 0; i < prevActions.length; i++) {
                    prevActions[i][0] = actionHistory[(iterNo) - 1][i];
                    prevActions[i][1] = actionHistory[(iterNo) - 2][i];
                }

                for (int i = 0; i < prevActions.length; i++) {
                    if (prevActions[i][0].equals("DEPART") && prevActions[i][1].equals("PAY")) {
                        randNo = SimConst.myRand.nextInt(100);
                        if (randNo < SimConst.punishProbForNonTipping * 100) {
                            nonTippersList.add(i);
                        }

                    }
                }
            }
            return nonTippersList;
        }
    }
}
