/*
 * This is the "model" part of the MVC architecture
 */
package domain;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

/**
 *
 * @author TonyR
 */
public class WorldModel extends Observable {

    private static int currentPos[][] = new int[SimConst.noOfAgents][2];
    private static int chairsPos[][] = null;
    private static int tablesPos[][] = new int[SimConst.noOfTableColumns * SimConst.noOfTableRows][4];
    private static String personalityArr[] = null;
    private static SimConst.Action currentActionArr[] = new SimConst.Action[SimConst.noOfAgents];
    private static int iterationNo = -1;
    private static List<String> normsArrList[] = new ArrayList[SimConst.noOfAgents];

    public void initializeModel(int tableLocations[][], int chairsLocations[][], String personalities[]) {
        tablesPos = tableLocations;
        chairsPos = chairsLocations;
        personalityArr = personalities;
    }

    public void setPosition(int agentId, int x, int y) {
        currentPos[agentId][0] = (x);
        currentPos[agentId][1] = (y);
        setChanged();
        notifyObservers();
    }

    public Dimension getPosition(int agentId) {
        return new Dimension(currentPos[agentId][0], currentPos[agentId][1]);
    }

    public int[][] getChairsPos() {
        return chairsPos;
    }

    public int[][] getTablesPos() {
        return tablesPos;
    }

    public String getPeronality(int id) {
        if(personalityArr !=null ) {
            return personalityArr[id];
        }
        else
        {
            return null;
        }
    }

    public void setAction(int agentId, SimConst.Action action) {
        currentActionArr[agentId] = action;
    }

    public static SimConst.Action getAction(int agentId) {
        return currentActionArr[agentId];
    }

    public static String getActionAsString(int agentId) {
        if (currentActionArr[agentId] != null) {
            return currentActionArr[agentId].toString();
        } else {
            return "empty";
        }
    }

    public void setIterationNo(int iterNo) {
        this.iterationNo = iterNo;
    }

    public String getIterationNo() {
        return String.valueOf(iterationNo + 1);
    }

    public static boolean isFree(int x, int y) {
        boolean tmpBool = true;

        for (int i = 0; i < SimConst.noOfAgents; i++) {
            if (currentPos[i][0] == x & currentPos[i][1] == y) {
                return false;
            }
        }

        for (int i = 0; i < tablesPos.length; i++) {
            if (x >= tablesPos[i][0] && x <= tablesPos[i][0] + tablesPos[i][2] && y >= tablesPos[i][1] && y <= tablesPos[i][1] + tablesPos[i][2]) {
                return false;
            }
        }

        for (int i = 0; i < chairsPos.length; i++) {
            if (chairsPos[i][0] == x && chairsPos[i][1] == y) {
                return false;
            }
        }

        return tmpBool;
    }

    public static Dimension[] getAllPathStartsWithinZone(int zone) {
        Dimension startPaths[] = new Dimension[4];
        int counter = 0;
        for (int i = 0; i < chairsPos.length; i++) {
            if (chairsPos[i][0] == SimConst.dbt_x && chairsPos[i][2] == zone) {
                if (counter % 2 == 0) {
                    startPaths[counter] = new Dimension(chairsPos[i][0], chairsPos[i][1] - 1);
                } else {
                    startPaths[counter] = new Dimension(chairsPos[i][0], chairsPos[i][1] + 1);
                }
                counter++;
            }
        }

        for (int i = 0; i < startPaths.length; i++) {
            Dimension dimension = startPaths[i];
        }

        return startPaths;
    }

    public void addNorm(int agentId, String candidateNorm) {
        if (normsArrList[agentId] == null) {
            normsArrList[agentId] = new ArrayList();
            normsArrList[agentId].add(candidateNorm);

        } else {
            List normsList = normsArrList[agentId];
            boolean normExists = false;

            for (int i = 0; i < normsList.size(); i++) {
                if (normsList.get(i).equals(candidateNorm)) {
                    normExists = true;
                    break;
                }
            }

            if (!normExists) {
                normsArrList[agentId].add(candidateNorm);
            }
        }
    }

    public List getNorms(int agentId) {
        List normsList = null;

        if (normsArrList[agentId] != null) {
            normsList = normsArrList[agentId];
        }

        return normsList;

    }
    
    public void deleteNorms(int agentId) {
        if(normsArrList[agentId]!=null) {
        normsArrList[agentId].clear(); }
    }
}
