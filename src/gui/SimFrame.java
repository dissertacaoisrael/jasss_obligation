/**
 *
 * SimFrame contains SimJPanel which is the view of the "MVC" architecture.
 */

package gui;

import domain.SimConst;
import domain.WorldModel;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;

public class SimFrame extends javax.swing.JFrame  {
    SimJPanel panel = null;
  
    /** Creates new form SimFrame */
    public SimFrame(WorldModel model) {
        initComponents();
        this.setSize(SimConst.gridSize + 15,SimConst.gridSize + 40);
        panel = new SimJPanel(model,this);
        this.add(panel, BorderLayout.CENTER);
        model.addObserver(panel);
        Dimension screenCentre = this.getScreenLocationAtCentre();
        this.setLocation(screenCentre.width, screenCentre.height);
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Norms simulator");
        getContentPane().setLayout(null);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
    * @param args the command line arguments
    */
   
   
    public Dimension getScreenLocationAtCentre() {
       int w =  this.getSize().width;
       int h =  this.getSize().height;
       
       // Get the size of the screen
       Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
       
        int x = (dim.width-w)/2;
        int y = (dim.height-h)/2;
        
        return new Dimension(x,y);
    }
    


    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables

}
