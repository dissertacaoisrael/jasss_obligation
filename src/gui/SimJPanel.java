/** SimJPanel is the observer of the WorldModel class. When the data changes the SimJPanel is redrawn.
 * For example, when an agent moves from one location to another, the SimJPanel is redrawn.
 * SimJPanel is the view in the "MVC" architecture, which gets updates when the model changes.
 */

package gui;

import domain.SimConst;
import domain.WorldModel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.Observable;
import java.util.Observer;

public class SimJPanel extends javax.swing.JPanel implements Observer {

    WorldModel model = null;
    SimFrame frame = null;

    /** Creates new form SimJPanel */
    public SimJPanel(WorldModel model, SimFrame outerFrame) {
        this.setLayout(null);
        this.model = model;
        frame = outerFrame;
        this.setSize(SimConst.gridSize+10, SimConst.gridSize +10);
    }

    @Override
    protected void paintComponent(Graphics g) {
        //redraw grid every time
        g.clearRect(0, 0, SimConst.gridSize+10, SimConst.gridSize+10);
        this.setForeground(Color.WHITE);
        g.setColor(Color.LIGHT_GRAY);


        //draw the grid
        for (int i = 0; i <= SimConst.gridSize; i += SimConst.sizeOfCell) {
            g.drawLine(0, i, SimConst.gridSize, i);
            g.drawLine(i, 0, i, SimConst.gridSize);
        }

        //draw furnitures in the restaurant (table and chairs)
        g.setColor(Color.BLACK);

        // Get the table locations from model and draw it
        int tablesLocations[][] = model.getTablesPos();
        for (int i = 0; i < tablesLocations.length; i++) {
            g.fillRect(tablesLocations[i][0] * SimConst.sizeOfCell, tablesLocations[i][1] * SimConst.sizeOfCell, tablesLocations[i][2] * SimConst.sizeOfCell, tablesLocations[i][3] * SimConst.sizeOfCell);

        }

        //Get the chair locations from the model and draw it
        int chairsPos[][] = model.getChairsPos();
        if(chairsPos!=null) {
            for (int i = 0; i < chairsPos.length; i++) {
                g.fillOval(chairsPos[i][0] * SimConst.sizeOfCell, chairsPos[i][1] * SimConst.sizeOfCell, SimConst.sizeOfCell, SimConst.sizeOfCell);
            }
        }

        //draw all agents
        for (int i = 0; i < SimConst.noOfAgents; i++) {
            if(model.getPeronality(i)!=null) {
                Dimension dim = model.getPosition(i);
                g.setColor(getAgentColor(model.getPeronality(i)));
                g.fillOval(dim.width * SimConst.sizeOfCell, dim.height * SimConst.sizeOfCell, SimConst.sizeOfCell, SimConst.sizeOfCell);
                g.drawString(i + 1 + "[" + model.getActionAsString(i).charAt(0) + "]", dim.width * SimConst.sizeOfCell, (dim.height * SimConst.sizeOfCell));

                //if an agent has a norm then draw a square inside the agent with an appropriate color
                if (model.getNorms(i) != null) {
                    if (model.getNorms(i).contains("obliged(T)")) {
                        g.setColor(SimConst.TipNormColor);
                        g.fillRect((dim.width * SimConst.sizeOfCell) + SimConst.sizeOfCell / 4, (dim.height * SimConst.sizeOfCell) + SimConst.sizeOfCell / 4, SimConst.sizeOfCell / 4, SimConst.sizeOfCell / 4);
                    }
                }
            }
        }
        frame.setTitle("Iteration no " + model.getIterationNo());
    }

    public Color getAgentColor(String personality) {
        if (personality.equals("TC")) {
            return SimConst.TCColor;
        } else if (personality.equals("NTC")) {
            return SimConst.NTCColor;
        } else {
            return SimConst.WColor;
        }
    }

    public void update(Observable o, Object arg) {
        repaint();
    }
}
