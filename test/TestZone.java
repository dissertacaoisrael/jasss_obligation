/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.Test;
import static org.junit.Assert.*; 

/**
 *
 * @author TonyR
 */
public class TestZone {

    public TestZone() {
    }
    @Test
    public void testZone() {
      assertEquals(getZone(10,10),1);
      assertEquals(getZone(24,24),1);
      assertEquals(getZone(24,25),2);
      assertEquals(getZone(49,49),1);
    }
    
    int noOfAgents = 50;
    
    public int getZone(int x, int y) {
        int maxXYPos = noOfAgents-1; //This is the maximum X and Y Pos an agent can have
        int midXYPos = (noOfAgents/2)-1;
        int minXYPos = 0;
        int zone = -1;
        
        System.out.println(minXYPos + " " + midXYPos + " " + maxXYPos);
        if(isWithin(minXYPos, minXYPos, maxXYPos,  midXYPos, x,y)) {
            zone = 1;
        }
        else if(isWithin(minXYPos,midXYPos, maxXYPos, maxXYPos, x, y)) {
            zone = 2;
        }
        return zone;
    }
    
    
    public boolean isWithin(int x1, int y1, int x2, int y2, int x, int y) {
        System.out.println("Values of x and y are " + x + " " + y);
        System.out.println("Values of x and y are " + x1 + " " + x2 + " " + y1 + " " + y2);
        if(x >= x1 && x <=x2 && y >=y1 && y<=y2) {
            return true;
        }
        else
        {
            return false;
        }
    }


}